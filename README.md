# Inkscape-Extensions

- [ColorPaletteFromImage](https://gitlab.com/HarshilPatel007/inkscape-extensions/-/tree/ColorPaletteFromImage "ColorPaletteFromImage")
- [ColorPaletteFromSVG](https://gitlab.com/HarshilPatel007/inkscape-extensions/-/tree/ColorPaletteFromSVG "ColorPaletteFromSVG")
- [CircularPolarGuides](https://gitlab.com/HarshilPatel007/inkscape-extensions/-/tree/CircularPolarGuides "CircularPolarGuides")
